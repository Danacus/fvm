package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"runtime"
	"strings"
	"text/template"

	"github.com/spf13/viper"
	"gopkg.in/urfave/cli.v1"
)

type gameListTemplate struct {
	Name    string
	Version string
}

type modpackListTemplate struct {
	Name string
	Game string
}

type osDefaults struct {
	configPath    string
	dataPath      string
	extractCmd    func(file, targetPath string) *exec.Cmd
	runCmd        func(game string) *exec.Cmd
	runModpackCmd func(game, pack string) *exec.Cmd
}

type modPackInfo struct {
	Game string
}

type gameInfo struct {
	Version string
}

var linuxDefaults = osDefaults{
	configPath: "$HOME/.config/fvm/",
	dataPath:   "$HOME/.local/share/fvm",
	extractCmd: func(file, targetPath string) *exec.Cmd {
		return exec.Command("tar", "xf", file, "-C", targetPath, "--strip-components=1")
	},
	runCmd: func(game string) *exec.Cmd {
		return exec.Command(path.Join(getGamePath(game), "/bin/x64/factorio"))
	},
	runModpackCmd: func(game, pack string) *exec.Cmd {
		return exec.Command(path.Join(getGamePath(game), "/bin/x64/factorio"), "--mod-directory", path.Join(getModpackPath(pack)))
	},
}

var windowsDefaults = osDefaults{
	configPath: "$APPDATA/fvm/",
	dataPath:   "$APPDATA/fvm",
	extractCmd: func(file, targetPath string) *exec.Cmd {
		return nil
	},
}

var macOSDefaults = osDefaults{}

var defaults osDefaults

func main() {
	app := cli.NewApp()
	switch runtime.GOOS {
	case "linux":
		defaults = linuxDefaults
	case "windows":
		defaults = windowsDefaults
	case "darwin":
		defaults = macOSDefaults
	}

	app.Name = "Factorio Version Manager"
	app.Usage = "Manage installed versions of the game"
    app.Version = "0.0.1"
	app.Commands = []cli.Command{
		{
			Aliases: []string{"g"},
			Subcommands: []cli.Command{
				{
					Name: "install",
					Usage: "Install a new version from the given archive, directory, or executable.\n" +
						"\tUsage: fvm game install <path to archive> <name for the instance>",
					Action: installGame,
				},
				{
					Name: "remove",
					Usage: "Remove an installed version of the game (Warning: saves for this version will be lost!).\n" +
						"\tUsage: fvm game remove <name of the instance>",
					Action: removeGame,
				},
				{
					Name: "list",
					Usage: "Lists installed game versions of the game.\n" +
						"\tFormat options:\n" +
						"\t{{.Name}}    The name of the instance\n" +
						"\t{{.Version}} The version of the instance\n" +
						"\tDefault format:\n" +
						"\t{{.Name}} - {{.Version}}\n",

					Action: listGames,
				},
				{
					Name:    "run",
					Aliases: []string{"r"},
					Usage: "Runs the game with default settings.\n" +
						"\tUsage: fvm game run <name of the game instance>",
					Action: runGame,
				},
			},
		},
		{
			Name:    "saves",
			Aliases: []string{"s"},
			Usage:   "Manage your save files.",
		},
		{
			Name:    "modpack",
			Aliases: []string{"mp"},
			Usage:   "Manage your installed modpacks.",
			Subcommands: []cli.Command{
				{
					Name: "create",
					Usage: "Creates a new empty modpack with the given name and game instance.\n" +
						"\tUsage: fvm modpack create <name for the modpack>",
					Action: createModpack,
				},
				{
					Name: "remove",
					Usage: "Removes the given modpack.\n" +
						"\tUsage: fvm modpack remove <name of the modpack>",
					Action: "removemp",
				},
				{
					Name: "list",
					Usage: "Lists the installed modpacks.\n" +
						"\tFormat options:\n" +
						"\t{{.Name}}    The name of the modpack\n" +
						"\t{{.Game}} The game instance used to run the modpack\n" +
						"\tDefault format:\n" +
						"\t{{.Name}} - {{.Game}}\n",
					Action: listModpacks,
				},
				{
					Name: "run",
					Usage: "Runs the given modpack with the configured game instance.\n" +
						"\tUsage: fvm modpack run <name of the modpack>",
					Action: runModpack,
				},
			},
		},
	}

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config",
			Value: defaults.configPath,
			Usage: "Path to the config directory.",
		},
		cli.StringFlag{
			Name:  "format",
			Value: "",
			Usage: `Format used to display the output of list commands.
                    Example:

                    fvm game list --format "Factorio - {{.Name}} - {{.Version}}"

                    will output something like:

                    Factorio - Experimental - 0.17.62
                    Factorio - Stable - 0.16.51
                    `,
		},
	}

	app.Before = func(c *cli.Context) error {
		readConfig(c.GlobalString("config"))
		dataPath := os.ExpandEnv(viper.GetString("path"))
		setupDataDir(dataPath)
		return nil
	}

	app.Action = func(c *cli.Context) error {
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

/*
 * 	Helper functions
 */

func readConfig(configPath string) {
	viper.SetDefault("path", defaults.dataPath)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(configPath)

	viper.ReadInConfig()
}

func setupDataDir(dataPath string) {
	os.MkdirAll(path.Join(dataPath, "/instances/"), os.ModePerm)
	os.MkdirAll(path.Join(dataPath, "/modpacks/"), os.ModePerm)
}

func getGamePath(name string) string {
	return path.Join(os.ExpandEnv(viper.GetString("path")), "/instances/", name)
}

func getModpackPath(name string) string {
	return path.Join(os.ExpandEnv(viper.GetString("path")), "/modpacks/", name)
}

func getGameVersion(gamePath string) string {
	file := path.Join(gamePath, "/data/base/info.json")
	jsonbytes, err := ioutil.ReadFile(file)
	jsonfile := string(jsonbytes)

	if err != nil {
		panic(err)
	}

	dec := json.NewDecoder(strings.NewReader(jsonfile))

	var info gameInfo
	err = dec.Decode(&info)

	if err != nil {
		panic(err)
	}

	return info.Version
}

func getModpackGame(packPath string) string {
	file := path.Join(getModpackPath(packPath), "config.json")
	jsonbytes, err := ioutil.ReadFile(file)
	jsonfile := string(jsonbytes)

	if err != nil {
		panic(err)
	}

	dec := json.NewDecoder(strings.NewReader(jsonfile))

	var info modPackInfo
	err = dec.Decode(&info)

	if err != nil {
		panic(err)
	}

	return info.Game
}

/*
 * 	Game functions
 */

func installGame(c *cli.Context) error {
	file := c.Args().Get(0)
	name := c.Args().Get(1)

	if _, err := os.Stat(file); os.IsNotExist(err) {
		log.Fatalln("File not found.")
	}

	if name == "" {
		log.Fatalln("No name supplied.")
	}

	targetPath := path.Join(os.ExpandEnv(viper.GetString("path")), "/instances/", name)
	os.Mkdir(targetPath, os.ModePerm)
	cmd := defaults.extractCmd(file, targetPath)

	fmt.Println("Extracting archive ...")
	err := cmd.Run()

	if err != nil {
		log.Fatalln("Failed to extract archive.")
	} else {
		fmt.Println("Done.")
	}

	return nil
}

func removeGame(c *cli.Context) error {
	name := c.Args().Get(0)

	if name == "" {
		log.Fatalln("No name supplied.")
	}

	if _, err := os.Stat(getGamePath(name)); os.IsNotExist(err) {
		log.Fatalln("Given game does not exist.")
	}

	err := os.RemoveAll(getGamePath(name))

	if err != nil {
		log.Fatalln("Failed to remove game.")
	} else {
		fmt.Println("Done.")
	}

	return nil
}

func listGames(c *cli.Context) error {
	format := "{{.Name}} - {{.Version}}"

	if c.GlobalString("format") != "" {
		format = c.GlobalString("format")
	}

	format += "\n"

	tmpl := template.New("listoutput")
	tmpl, err := tmpl.Parse(format)

	if err != nil {
		panic("invalid output pattern: " + err.Error())
	}

	root := os.ExpandEnv(path.Join(viper.GetString("path"), "/instances"))
	files, err := ioutil.ReadDir(root)
	if err != nil {
		panic(err)
	}

	for _, file := range files {
		listtmpl := gameListTemplate{
			Name:    file.Name(),
			Version: getGameVersion(path.Join(root, file.Name())),
		}

		tmpl.Execute(os.Stdout, listtmpl)
	}

	return nil
}

func runGame(c *cli.Context) error {
	game := c.Args().Get(0)

	if game == "" {
		log.Fatalln("No game supplied.")
	}

	if _, err := os.Stat(getGamePath(game)); os.IsNotExist(err) {
		log.Fatalln("Given game does not exist.\n Use 'fvm game install <game archive> " + game + "' to install a new game instance.")
	}

	cmd := defaults.runCmd(game)
	fmt.Println("Running game ...")
	err := cmd.Run()

	if err != nil {
		log.Fatalln("Failed to start factorio.")
	} else {
		fmt.Println("Done.")
	}
	return nil
}

/*
 * 	Modpack functions
 */

func createModpack(c *cli.Context) error {
	name := c.Args().Get(0)
	if name == "" {
		log.Fatalln("No name supplied.")
	}

	game := c.Args().Get(1)
	if game == "" {
		log.Fatalln("No game supplied.")
	}

	os.Mkdir(getModpackPath(name), os.ModePerm)

	b, err := json.Marshal(modPackInfo{Game: game})

	if err != nil {
		panic(err)
	}

	ioutil.WriteFile(path.Join(getModpackPath(name), "config.json"), b, os.ModePerm)

	return nil
}

func listModpacks(c *cli.Context) error {
	format := "{{.Name}} - {{.Game}}"

	if c.GlobalString("format") != "" {
		format = c.GlobalString("format")
	}

	format += "\n"

	tmpl := template.New("listoutput")
	tmpl, err := tmpl.Parse(format)

	if err != nil {
		panic("invalid output pattern: " + err.Error())
	}

	root := os.ExpandEnv(path.Join(viper.GetString("path"), "/modpacks/"))
	files, err := ioutil.ReadDir(root)
	if err != nil {
		panic(err)
	}

	for _, file := range files {
		listtmpl := modpackListTemplate{
			Name: file.Name(),
			Game: getModpackGame(file.Name()),
		}

		tmpl.Execute(os.Stdout, listtmpl)
	}

	return nil
}

func runModpack(c *cli.Context) error {
	pack := c.Args().Get(0)

	if pack == "" {
		log.Fatalln("No modpack supplied.")
	}

	if _, err := os.Stat(getModpackPath(pack)); os.IsNotExist(err) {
		log.Fatalln("Given modpack does not exist.\n Use 'fvm modpack create " + pack + "' to create the modpack")
	}

	cmd := defaults.runModpackCmd(getModpackGame(pack), pack)
	fmt.Println("Running game ...")
	err := cmd.Run()

	if err != nil {
		log.Fatalln("Failed to start factorio.")
	} else {
		fmt.Println("Done.")
	}
	return nil
}

func removeModpack(c *cli.Context) error {
	pack := c.Args().Get(0)

	if pack == "" {
		log.Fatalln("No modpack supplied.")
	}

	if _, err := os.Stat(getModpackPath(pack)); os.IsNotExist(err) {
		log.Fatalln("Given modpack does not exist.")
	}

	err := os.RemoveAll(getModpackPath(pack))

	if err != nil {
		log.Fatalln("Failed to remove the pack.")
	} else {
		fmt.Println("Done.")
	}

	return nil
}
